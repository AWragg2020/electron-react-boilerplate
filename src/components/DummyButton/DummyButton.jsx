import React from "react";

const DummyButton = (props) => {
  const onDummyButtonClick = () => {
    props.onClick();
    console.log("Dummy button has been clicked");
  };

  return <button onClick={onDummyButtonClick}>Click me</button>;
};

export default DummyButton;
