import React, { useState } from "react";
import DummyButton from "./components/DummyButton/DummyButton.jsx";

const App = () => {
  const [count, setCount] = useState(0);
  const onDummyButtonClick = () => {
    setCount(count + 1);
  };

  return (
    <>
      <h2>Hello from React</h2>
      <p>Count: {count}</p>
      <DummyButton onClick={onDummyButtonClick} />
    </>
  );
};

export default App;
